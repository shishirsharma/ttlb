
class Player
  include DataMapper::Resource

  property :id, Serial

  property :name, String, :required => true 

  property :email, String, :format => :email_address

  property :points, Integer, :default => 25

  property :attacker_points, Integer, :default => 25
  property :defender_points, Integer, :default => 25

  property :created_at, DateTime, :index => true
  property :updated_at, DateTime, :index => true

  has n, :teams, :child_key => [:attacker_id, :defender_id]
end
